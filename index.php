<!doctype html>
<html lang="en">

<head>

    <title>HOME</title>

    <link href="https://getbootstrap.com/docs/5.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
</head>

<body>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4">
        <div class="container-fluid">
            <a class="navbar-brand" href="index.php">ITM_SRL</a>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav me-auto mb-2 mb-md-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="contact.html">ASSUNZIONI</a>
                    </li>
                    
                    <li class="nav-item">
                        <a class="nav-link active" href="query.php">TABELLA</a>
                    </li>

                </ul>

            </div>
        </div>
    </nav>

    <main class="container">
        <div class="bg-light p-5 rounded">
            <h1>Visualizza tabella dipendenti</h1>
            <p class="lead">Gestione del'azienda visualizzazione inserimento nuovi dipendenti </p>
            <a class="btn btn-lg btn-primary" href="query.php" role="button">Tabella</a>
        </div>
    </main>

</body>

</html>